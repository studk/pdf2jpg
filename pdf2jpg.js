const path = require('path');
const pdf = require('pdf-poppler');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs')
const resizeImg = require('resize-img');


// define file path
let pdfpath = 'E:\\1stAPP\\pdf2jpg\\pdf\\'
let jpgpath = 'E:\\1stAPP\\pdf2jpg\\thumbnails\\'
let files = fs.readdirSync(pdfpath)
console.log(files)

// loop through pdf folder
for (i = 0; i < files.length; i++){
  let filename = uuidv4();
const convert = async () => {
  try {
    // rename pdf with UUID
    fs.renameSync(pdfpath+files[i], pdfpath + filename + '.pdf');

    let file = pdfpath + filename + '.pdf'

    let opts = {
      format: 'jpeg',
      scale: 206,
      out_dir: jpgpath,
      out_prefix: filename,
      // out_prefix: path.basename(file, path.extname(file)),
      page: 1
    }

    // convert pdf to jpg
    pdf.convert(file, opts)
      .then(res => {
        console.log('Successfully converted');

      // rename jpg to match with its pdf's UUID
        fs.renameSync(jpgpath + filename + '-1.jpg', jpgpath + filename + '.jpg');
      })
      .catch(error => {
        console.error(error);
      })
    
    
    
    
  
  } catch (error) {
    console.error(error)
  }
}
convert();
}
